// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "CMovingPlatform.generated.h"

/**
 * 
 */
UCLASS()
class PUZZLEPLATFORMS_API ACMovingPlatform : public AStaticMeshActor
{
	GENERATED_BODY()

public:

	ACMovingPlatform();

private:

	FVector StartLocation, EndLocation;

	bool bDirection;

	UPROPERTY(EditInstanceOnly, Category = "Moving")
	uint8 ActiveTriggers;

	FTimerHandle TimerHandle_Move;

protected:

	virtual void BeginPlay() override;

	void MoveToTarget();

protected:

	UPROPERTY(EditInstanceOnly, Category = "Moving", Meta = (MakeEditWidget = true))
	FVector TargetLocation;

	UPROPERTY(EditInstanceOnly, Category = "Moving")
	float Speed;

public:

	void AddActiveTrigger();

	void RemoveActiveTrigger();
};
