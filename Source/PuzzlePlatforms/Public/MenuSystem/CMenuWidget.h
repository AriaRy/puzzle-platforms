﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "CMenuWidget.generated.h"

class ICMenuInterface;
/**
 * 
 */
UCLASS()
class PUZZLEPLATFORMS_API UCMenuWidget : public UUserWidget
{
	GENERATED_BODY()

protected:

	virtual void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld) override;

public:

	void Setup();

	void TearDown();

	void SetMenuInterface(ICMenuInterface* MenuInterface);

protected:

	ICMenuInterface* MenuInterface;
};
