// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"

#include "CServerRow.generated.h"

class UButton;
class UCMainMenu;
/**
 * 
 */
UCLASS()
class PUZZLEPLATFORMS_API UCServerRow : public UUserWidget
{
	GENERATED_BODY()

public:

	virtual bool Initialize() override;

	FText GetServerName() const { return ServerName->GetText(); }

	void SetServerName(const FText& ServerName) const;

	void Setup(UCMainMenu* Parent, uint32 Index);

private:

	UPROPERTY(meta = (BindWidget))
	UTextBlock* ServerName;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* Players;

	UPROPERTY(meta = (BindWidget))
	UButton* Button;

	UPROPERTY()
	UCMainMenu* Parent;

	uint32 Index;

private:

	UFUNCTION()
	void OnClicked();

};
