// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "CMenuWidget.h"
#include "CMainMenu.generated.h"

class UCServerRow;
class UScrollBox;
class UEditableTextBox;
class UWidgetSwitcher;
class UButton;
/**
 * 
 */
UCLASS()
class PUZZLEPLATFORMS_API UCMainMenu : public UCMenuWidget
{
	GENERATED_BODY()

public:

	UCMainMenu(const FObjectInitializer& ObjectInitializer);

	virtual bool Initialize() override;

	void SetServerList(const TArray<FString>& ServerNames);

	void ClearServerList() const;

	void SelectServerIndex(uint32 Index);

private:

	TSubclassOf<UCServerRow> ServerRowClass;

	UPROPERTY(meta = (BindWidget))
	UButton* HostButton;

	UPROPERTY(meta = (BindWidget))
	UButton* JoinButton;

	UPROPERTY(meta = (BindWidget))
	UButton* ConfirmJoinMenuButton;

	UPROPERTY(meta = (BindWidget))
	UButton* CancelJoinMenuButton;

	UPROPERTY(meta = (BindWidget))
	UButton* QuitGameButton;

	UPROPERTY(meta = (BindWidget))
	UWidgetSwitcher* MenuSwitcher;

	UPROPERTY(meta = (BindWidget))
	UScrollBox* ServerList_ScrollBox;

	UPROPERTY(meta = (BindWidget))
	UWidget* MainMenu;

	UPROPERTY(meta = (BindWidget))
	UWidget* JoinMenu;

	TOptional<uint32> SelectedServerIndex;

private:

	UFUNCTION()
	void HostButton_Clicked();

	UFUNCTION()
	void JoinButton_Clicked();

	UFUNCTION()
	void ConfirmJoinMenuButton_Clicked();

	UFUNCTION()
	void CancelJoinMenuButton_Clicked();

	UFUNCTION()
	void QuitGameButton_Clicked();

};
