// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "MenuSystem/CMenuInterface.h"

#include "CGameInstance.generated.h"

class UCInGameMenu;
class UCMainMenu;
/**
 * 
 */
UCLASS()
class PUZZLEPLATFORMS_API UCGameInstance : public UGameInstance, public ICMenuInterface
{
	GENERATED_BODY()

public:

	UCGameInstance(const FObjectInitializer& ObjectInitializer);

	virtual void Init() override;

	UFUNCTION(BlueprintCallable)
	void LoadMenu();

	UFUNCTION(BlueprintCallable)
	void LoadInGameMenu();

	virtual void Host() override;

	virtual void Join(uint32 Index) override;

	virtual void LoadMainMenu() override;

	virtual void QuitGame() override;

	virtual void RefreshServerList() override;

private:

	UCMainMenu* Menu;

	UCInGameMenu* InGameMenu;

	TSubclassOf<UCMainMenu> MainMenuClass;

	TSubclassOf<UCInGameMenu> InGameMenuClass;

	IOnlineSessionPtr SessionInterface;

	TSharedPtr<FOnlineSessionSearch> SessionSearch;

private:

	void OnCreateSessionComplete(FName SessionName, bool bWasSuccessful);

	void OnDestroySessionComplete(FName SessionName, bool bWasSuccessful);

	void CreateSession(const FName& SessionName);

	void OnFindSessionComplete(bool bWasSuccessful);

	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);

};
