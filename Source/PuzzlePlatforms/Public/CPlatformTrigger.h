// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPlatformTrigger.generated.h"

class ACMovingPlatform;
class UBoxComponent;

UCLASS()
class PUZZLEPLATFORMS_API ACPlatformTrigger : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACPlatformTrigger();

protected:

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UBoxComponent* BoxOverlap;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditInstanceOnly, Category = "Platforms")
	TArray<ACMovingPlatform*> PlatformsToTrigger;

protected:

	virtual void BeginPlay() override;

	UFUNCTION()
	void OnActivated(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	void OnDeactivated(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

};
