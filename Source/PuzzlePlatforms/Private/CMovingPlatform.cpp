// Fill out your copyright notice in the Description page of Project Settings.


#include "CMovingPlatform.h"

#include "DrawDebugHelpers.h"

ACMovingPlatform::ACMovingPlatform()
{
	SetMobility(EComponentMobility::Movable);

	Speed = 2.0f;

	// Change to 0 in Instance if you want to make in deactivated at first
	ActiveTriggers = 1;

	SetReplicates(true);
	ACMovingPlatform::SetReplicateMovement(true);
}

void ACMovingPlatform::BeginPlay()
{
	Super::BeginPlay();

	StartLocation = GetActorLocation();
	EndLocation = GetTransform().TransformPosition(TargetLocation);

	if (HasAuthority() && ActiveTriggers)
        GetWorldTimerManager().SetTimer(TimerHandle_Move, this, &ACMovingPlatform::MoveToTarget, 0.02f, true, 0.0f);
}

void ACMovingPlatform::MoveToTarget()
{
	const FVector Target = bDirection ? EndLocation : StartLocation;
	const FVector CurrentLocation = GetActorLocation();
	FVector Direction = Target - CurrentLocation; 
	Direction.Normalize();

	SetActorLocation(CurrentLocation + Direction * Speed);

	if (GetActorLocation().Equals(Target, 5.0f))
		bDirection = !bDirection;
}

void ACMovingPlatform::AddActiveTrigger()
{
	if (HasAuthority() && !ActiveTriggers)
        GetWorldTimerManager().SetTimer(TimerHandle_Move, this, &ACMovingPlatform::MoveToTarget, 0.02f, true, 0.0f);

	ActiveTriggers++;
}

void ACMovingPlatform::RemoveActiveTrigger()
{
	ActiveTriggers = FMath::Max(0, ActiveTriggers - 1);
	if (HasAuthority() && !ActiveTriggers)
		GetWorldTimerManager().ClearTimer(TimerHandle_Move);
}
