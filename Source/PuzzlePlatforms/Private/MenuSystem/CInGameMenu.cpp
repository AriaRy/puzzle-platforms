// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuSystem/CInGameMenu.h"

#include "Components/Button.h"
#include "MenuSystem/CMenuInterface.h"

bool UCInGameMenu::Initialize()
{
	if (!Super::Initialize()) return false;

	bIsFocusable = true;

	if (!ensureAlways(
		CancelButton &&
		QuitButton
	)) return false;

	CancelButton->OnClicked.AddDynamic(this, &UCInGameMenu::CancelButton_Clicked);
	QuitButton->OnClicked.AddDynamic(this, &UCInGameMenu::QuitButton_Clicked);

	return true;
}

void UCInGameMenu::CancelButton_Clicked()
{
	TearDown();
}

#pragma warning (disable : 4458)
void UCInGameMenu::QuitButton_Clicked()
{
	if (MenuInterface)
		MenuInterface->LoadMainMenu();
}
#pragma warning (default : 4458)
