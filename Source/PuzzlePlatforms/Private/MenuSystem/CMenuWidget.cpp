﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuSystem/CMenuWidget.h"

#pragma warning (disable : 4458)
void UCMenuWidget::SetMenuInterface(ICMenuInterface* MenuInterface)
{
	this->MenuInterface = MenuInterface;
}
#pragma warning (default : 4458)

void UCMenuWidget::OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld)
{
	Super::OnLevelRemovedFromWorld(InLevel, InWorld);

	TearDown();
}

void UCMenuWidget::Setup()
{
	this->AddToViewport();

	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	if (PlayerController)
	{
		FInputModeUIOnly InputModeData;
		InputModeData.SetWidgetToFocus(this->TakeWidget());
		InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

		PlayerController->SetInputMode(InputModeData);

		PlayerController->bShowMouseCursor = true;
	}
}

void UCMenuWidget::TearDown()
{
	this->RemoveFromParent();

	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	if (PlayerController)
	{
		const FInputModeGameOnly InputModeData;

		PlayerController->SetInputMode(InputModeData);

		PlayerController->bShowMouseCursor = false;
	}
}
