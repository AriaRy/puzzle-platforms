// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuSystem/CServerRow.h"

#include "Components/Button.h"
#include "MenuSystem/CMainMenu.h"

bool UCServerRow::Initialize()
{
	if (!Super::Initialize()) return false;

	if (!ensureAlways(
		ServerName &&
		Players &&
		Button
	)) return false;

	Button->OnClicked.AddDynamic(this, &UCServerRow::OnClicked);

	return true;
}

#pragma warning (disable : 4458)
void UCServerRow::SetServerName(const FText& ServerName) const
{
	this->ServerName->SetText(ServerName);
}
#pragma warning (default : 4458)

#pragma warning (disable : 4458)
void UCServerRow::Setup(UCMainMenu* Parent, uint32 Index)
{
	this->Parent = Parent;
	this->Index = Index;
}
#pragma warning (default : 4458)

void UCServerRow::OnClicked()
{
	if (Parent)
		Parent->SelectServerIndex(Index);
}
