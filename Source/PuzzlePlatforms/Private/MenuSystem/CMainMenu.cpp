// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuSystem/CMainMenu.h"

#include "Components/Button.h"
#include "Components/ScrollBox.h"
#include "Components/WidgetSwitcher.h"
#include "MenuSystem/CMenuInterface.h"
#include "MenuSystem/CServerRow.h"

UCMainMenu::UCMainMenu(const FObjectInitializer& ObjectInitializer)
{
	static ConstructorHelpers::FClassFinder<UCServerRow> ServerRowBPClass(TEXT("/Game/UI/WBP_ServerRow"));
	ensureAlwaysMsgf(ServerRowClass = ServerRowBPClass.Class, TEXT("WBP_ServerRow not found!"));
}

bool UCMainMenu::Initialize()
{
	if (!Super::Initialize()) return false;

	bIsFocusable = true;

	if (!ensureAlways(
		HostButton &&
		JoinButton &&
		ConfirmJoinMenuButton &&
		CancelJoinMenuButton &&
		QuitGameButton &&
		MenuSwitcher &&
		ServerList_ScrollBox
	)) return false;

	HostButton->OnClicked.AddDynamic(this, &UCMainMenu::HostButton_Clicked);
	JoinButton->OnClicked.AddDynamic(this, &UCMainMenu::JoinButton_Clicked);
	ConfirmJoinMenuButton->OnClicked.AddDynamic(this, &UCMainMenu::ConfirmJoinMenuButton_Clicked);
	CancelJoinMenuButton->OnClicked.AddDynamic(this, &UCMainMenu::CancelJoinMenuButton_Clicked);
	QuitGameButton->OnClicked.AddDynamic(this, &UCMainMenu::QuitGameButton_Clicked);

	return true;
}

void UCMainMenu::SetServerList(const TArray<FString>& ServerNames)
{
	ClearServerList();

	uint32 i = 0;
	for (const FString& ServerName : ServerNames)
	{
		UCServerRow* ServerRow = CreateWidget<UCServerRow>(this, ServerRowClass, FName(*ServerName));
		if (ServerRow)
		{
			ServerRow->SetServerName(FText::FromString(ServerName));
			ServerRow->Setup(this, i++);

			ServerList_ScrollBox->AddChild(ServerRow);
		}
	}
}

void UCMainMenu::ClearServerList() const
{
	ServerList_ScrollBox->ClearChildren();
}

void UCMainMenu::SelectServerIndex(uint32 Index)
{
	SelectedServerIndex = Index;
}

void UCMainMenu::HostButton_Clicked()
{
	if (MenuInterface)
		MenuInterface->Host();
}

void UCMainMenu::JoinButton_Clicked()
{
	MenuSwitcher->SetActiveWidget(JoinMenu);
	if (MenuInterface)
		MenuInterface->RefreshServerList();
}

void UCMainMenu::ConfirmJoinMenuButton_Clicked()
{
	if (SelectedServerIndex.IsSet())
		if (MenuInterface)
			MenuInterface->Join(SelectedServerIndex.GetValue());
}

void UCMainMenu::CancelJoinMenuButton_Clicked()
{
	MenuSwitcher->SetActiveWidget(MainMenu);
}

void UCMainMenu::QuitGameButton_Clicked()
{
	if (MenuInterface)
		MenuInterface->QuitGame();
}
