// Fill out your copyright notice in the Description page of Project Settings.


#include "CGameInstance.h"

#include "OnlineSessionSettings.h"
#include "OnlineSubsystem.h"

#include "Kismet/KismetSystemLibrary.h"
#include "MenuSystem/CMenuWidget.h"
#include "MenuSystem/CMainMenu.h"
#include "MenuSystem/CInGameMenu.h"

const FName SESSION_NAME = FName("My Game");

UCGameInstance::UCGameInstance(const FObjectInitializer& ObjectInitializer)
{
	static ConstructorHelpers::FClassFinder<UCMainMenu> MainMenuBPClass(TEXT("/Game/UI/WBP_MainMenu"));
	ensureAlwaysMsgf(MainMenuClass = MainMenuBPClass.Class, TEXT("WBP_MainMenu not found!"));

	static ConstructorHelpers::FClassFinder<UCInGameMenu> InGameMenuBPClass(TEXT("/Game/UI/WBP_InGameMenu"));
	ensureAlwaysMsgf(InGameMenuClass = InGameMenuBPClass.Class, TEXT("WBP_InGameMenu not found!"));
}

void UCGameInstance::Init()
{
	IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get();
	if (OnlineSubsystem)
	{
		SessionInterface = OnlineSubsystem->GetSessionInterface();
		if (SessionInterface)
		{
			SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &UCGameInstance::OnCreateSessionComplete);
			SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(this, &UCGameInstance::OnDestroySessionComplete);
			SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &UCGameInstance::OnFindSessionComplete);
			SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this, &UCGameInstance::OnJoinSessionComplete);
		}
	}
}

void UCGameInstance::LoadMenu()
{
	Menu = CreateWidget<UCMainMenu>(this, MainMenuClass, FName("MainMenu"));
	if (Menu)
	{
		Menu->Setup();
		Menu->SetMenuInterface(this);
	}
}

void UCGameInstance::LoadInGameMenu()
{
	InGameMenu = CreateWidget<UCInGameMenu>(this, InGameMenuClass, FName("MainMenu"));
	if (InGameMenu)
	{
		InGameMenu->Setup();
		InGameMenu->SetMenuInterface(this);
	}
}

void UCGameInstance::Host()
{
	if (SessionInterface.IsValid())
	{
		if (SessionInterface->GetNamedSession(SESSION_NAME))
			SessionInterface->DestroySession(SESSION_NAME);
		else
			CreateSession(SESSION_NAME);
	}
}

void UCGameInstance::Join(uint32 Index)
{
	if (SessionInterface.IsValid() && SessionSearch)
		SessionInterface->JoinSession(0, SESSION_NAME, SessionSearch->SearchResults[Index]);
}

void UCGameInstance::LoadMainMenu()
{
	APlayerController* PlayerController = GetFirstLocalPlayerController(GetWorld());
	if (PlayerController)
		PlayerController->ClientTravel("/Game/Maps/MainMenu", TRAVEL_Absolute);
}

void UCGameInstance::QuitGame()
{
	APlayerController* PlayerController = GetFirstLocalPlayerController(GetWorld());
	if (PlayerController)
		UKismetSystemLibrary::QuitGame(GetWorld(), PlayerController, EQuitPreference::Quit, false);
}

void UCGameInstance::RefreshServerList()
{
	SessionSearch = MakeShareable(new FOnlineSessionSearch());
	if (SessionSearch.IsValid())
	{
		SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
		SessionSearch->MaxSearchResults = INT32_MAX;
		SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());
	}
}

void UCGameInstance::OnCreateSessionComplete(FName SessionName, bool bWasSuccessful)
{
	if (bWasSuccessful)
		GetWorld()->ServerTravel("/Game/ThirdPersonCPP/Maps/ThirdPersonExampleMap?listen");
}

void UCGameInstance::OnDestroySessionComplete(FName SessionName, bool bWasSuccessful)
{
	if (bWasSuccessful)
		CreateSession(SessionName);
}

void UCGameInstance::CreateSession(const FName& SessionName)
{
	if (SessionInterface.IsValid())
	{
		FOnlineSessionSettings SessionSettings;
		SessionSettings.NumPublicConnections = 4;
		SessionSettings.bShouldAdvertise = true;
		SessionSettings.bUsesPresence = true;

		SessionInterface->CreateSession(0, SessionName, SessionSettings);
	}
}

void UCGameInstance::OnFindSessionComplete(bool bWasSuccessful)
{
	if (bWasSuccessful && SessionSearch.IsValid() && Menu)
	{
		TArray<FString> ServerNames;
		for (const FOnlineSessionSearchResult& SearchResult : SessionSearch->SearchResults)
			ServerNames.AddUnique(SearchResult.GetSessionIdStr());

		Menu->SetServerList(ServerNames);
	}
}

void UCGameInstance::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
	if (Result == EOnJoinSessionCompleteResult::Success && SessionInterface.IsValid())
	{
		FString Address;
		if (SessionInterface->GetResolvedConnectString(SessionName, Address))
		{
			APlayerController* PlayerController = GetFirstLocalPlayerController(GetWorld());
			if (PlayerController)
				PlayerController->ClientTravel(Address, TRAVEL_Absolute);
		}
	}
}
