// Fill out your copyright notice in the Description page of Project Settings.


#include "CPlatformTrigger.h"


#include "CMovingPlatform.h"
#include "Components/BoxComponent.h"
#include "PuzzlePlatforms/PuzzlePlatformsCharacter.h"

// Sets default values
ACPlatformTrigger::ACPlatformTrigger()
{
	BoxOverlap = CreateDefaultSubobject<UBoxComponent>("BoxOverlap");
	BoxOverlap->SetBoxExtent(FVector(50.0f, 50.0f, 20.0f));
	RootComponent = BoxOverlap;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetupAttachment(RootComponent);

}

void ACPlatformTrigger::BeginPlay()
{
	if (HasAuthority())
	{
		BoxOverlap->OnComponentBeginOverlap.AddDynamic(this, &ACPlatformTrigger::OnActivated);
		BoxOverlap->OnComponentEndOverlap.AddDynamic(this, &ACPlatformTrigger::OnDeactivated);
	}
}

void ACPlatformTrigger::OnActivated(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && Cast<APuzzlePlatformsCharacter>(OtherActor))
		for (ACMovingPlatform* Platform : PlatformsToTrigger)
			Platform->AddActiveTrigger();
}

void ACPlatformTrigger::OnDeactivated(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && Cast<APuzzlePlatformsCharacter>(OtherActor))
		for (ACMovingPlatform* Platform : PlatformsToTrigger)
			Platform->RemoveActiveTrigger();
}

